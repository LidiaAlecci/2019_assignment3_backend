package org.pss2019.assignment3.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;

import org.pss2019.assignment3.helper.EntityManagerFactoryHelper;
import org.pss2019.assignment3.model.Course;

public class CourseRepositoryImpl implements CourseRepository {

	public Optional<Course> findById(String code){
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    Course course=null;
	    try {
	    	entityManager.getTransaction().begin();
	    	course= (Course) entityManager.createQuery("FROM Course WHERE code = '"+ code.toString()+"'", Course.class).getSingleResult();
	    	entityManager.getTransaction().commit();
		    entityManager.close();
		    return Optional.ofNullable(course);
	    } catch(NoResultException e){
	    	throw(e);
	    }
	}
	
	public Iterable<Course> findAll(){
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    entityManager.getTransaction().begin();
	    List<Course> courses = entityManager.createQuery("FROM Course", Course.class).getResultList();
	    entityManager.getTransaction().commit();
	    entityManager.close();
	    return courses;
	}
	
	public void create(String code, String name) {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		Course course = null;
		try {
			entityManager.getTransaction().begin();
			course= new Course(code,name);
			entityManager.persist(course);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch(RollbackException e) {
			  throw(e);
		  }
	}
	
	public void updateById(String code, Course course) {
		if(code.equals(course.getCode())) {
		  	final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
			entityManager.getTransaction().begin();
			Course courseOld = entityManager.find(Course.class, code);
			if(courseOld!=null) {
				courseOld=course;
			}
			entityManager.merge(course);
			entityManager.getTransaction().commit();
			entityManager.close();
		  }
	}
	
	public void remove(String code) {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    Course course=null;
	    try {
	    	entityManager.getTransaction().begin();
	    	Optional<Course> courseOp = findById(code);
	    	course=courseOp.get();
  	  		entityManager.remove(entityManager.contains(course) ? course : entityManager.merge(course));
  	  		entityManager.getTransaction().commit();
  	  		entityManager.close();
	    } catch(NoResultException e){
	    	throw(e);
	    }
	}
	
	public void removeAll() {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    entityManager.getTransaction().begin();
	    Iterable<Course> courses = findAll();
	    for(Course c: courses) {
	    	if(c.getProfessor() != null) {
	    		c.setProfessor(null);
	    	}
	    	if(!c.getPrerequisiteCourses().isEmpty()) {
	    		c.setPrerequisiteCourses(null);
	    	}
			entityManager.remove(entityManager.contains(c) ? c : entityManager.merge(c));
	    }
	    entityManager.getTransaction().commit();
	    entityManager.close();
	}

	public int size() {
		Iterable<Course> courses = findAll();
		return Math.toIntExact(StreamSupport.stream(courses.spliterator(), false).count());
	}
}
