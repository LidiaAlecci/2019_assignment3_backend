package org.pss2019.assignment3.repository;

import java.util.Date;
import java.util.Optional;

import org.pss2019.assignment3.model.Student;

public interface StudentRepository extends Repository<Student, Long>{

	Optional<Student> findById(Long id);

	Iterable<Student> findAll();
	
	Optional<Student> findByFc(String fc);
	
	void create(String fc,String firstName, String lastName, String city, Date birth, Long year);
	
	void updateById(Long id, Student stud);
	
	void remove(Long id);
	
	void removeAll();
	
	int size();
}