package org.pss2019.assignment3.repository;

import java.util.Date;
import java.util.Optional;

import org.pss2019.assignment3.model.Professor;

public interface ProfessorRepository extends Repository<Professor, Long>{
	
	Optional<Professor> findById(Long id);
	
	Iterable<Professor> findAll();
	
	Optional<Professor> findByFc(String fc);
		
	void create(String fc,String firstName, String lastName, String city, Date birth);

	void updateById(Long id, Professor prof);
	
	void remove(Long id);
	
	void removeAll();
	
	int size();
}
