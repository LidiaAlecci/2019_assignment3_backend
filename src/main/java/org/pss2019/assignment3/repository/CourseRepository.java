package org.pss2019.assignment3.repository;

import java.util.Optional;

import org.pss2019.assignment3.model.Course;

public interface CourseRepository extends Repository<Course, String>{

	Optional<Course> findById(String code);

	Iterable<Course> findAll();
		
	void create(String code, String name);
	
	void updateById(String code, Course course);
	
	void remove(String code);
	
	void removeAll();
	
	int size();
}