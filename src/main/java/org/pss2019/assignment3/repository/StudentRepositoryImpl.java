package org.pss2019.assignment3.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;

import org.pss2019.assignment3.helper.EntityManagerFactoryHelper;
import org.pss2019.assignment3.model.Student;

public class StudentRepositoryImpl implements StudentRepository {
	
	  public Optional<Student> findById(Long id) {
	    final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    entityManager.getTransaction().begin();
	    Student stud = entityManager.find(Student.class, id);
	    entityManager.getTransaction().commit();
	    entityManager.close();
	    return Optional.ofNullable(stud);
	  }
	  
	  public Iterable<Student> findAll() {
		    final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		    entityManager.getTransaction().begin();
		    List<Student> students = entityManager.createQuery("FROM Student", Student.class).getResultList();
		    entityManager.getTransaction().commit();
		    entityManager.close();
		    return students;
	  }
	  
	  public Optional<Student> findByFc(String fc) {
		    final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		    Student stud = null;
		    try {
		    	entityManager.getTransaction().begin();
			    stud = (Student) entityManager.createQuery("FROM Student WHERE fc = '"+ fc.toString()+"'", Student.class).getSingleResult();
			    entityManager.getTransaction().commit();
			    entityManager.close();
			    return Optional.ofNullable(stud);
		    } catch(NoResultException e){
		    	throw(e);
		    } 
	  }
	  
	  public void create(String fc,String firstName,
	            String lastName, String city, Date birth, Long year) {
		  final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		  Student stud = null;
		  try {
			  entityManager.getTransaction().begin();
			  stud= new Student(fc,firstName,lastName,city,birth,year);
			  entityManager.persist(stud);
			  entityManager.getTransaction().commit();
			  entityManager.close();
		  } catch(RollbackException e) {
			  throw(e);
		  }
	 }
	  
	  public void updateById(Long id, Student stud) {
		  if(id==stud.getId()) {
		  	final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
			entityManager.getTransaction().begin();
			Student studOld = entityManager.find(Student.class, id);
			if(studOld!=null) {
				studOld=stud;
			}
			entityManager.merge(stud);
			entityManager.getTransaction().commit();
			entityManager.close();
		  }
	  }
	  
	  public void remove(Long id) {
	      final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	      Student stud= null;
	      try{
	    	  entityManager.getTransaction().begin();
	    	  Optional<Student> studOp = findById(id);
		      stud=studOp.get();
	    	  entityManager.remove(entityManager.contains(stud) ? stud : entityManager.merge(stud));
		      entityManager.getTransaction().commit();
		      entityManager.close();
	      } catch(NoResultException e){
		    	throw(e);
		    }
	    }
	    
	    public void removeAll() {
	      final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	      entityManager.getTransaction().begin();
	      Iterable<Student> students = findAll();
	      for(Student s: students) {
				entityManager.remove(entityManager.contains(s) ? s : entityManager.merge(s));
	      }
	      entityManager.getTransaction().commit();
	      entityManager.close();
	    }

	    public int size() {
			Iterable<Student> students = findAll();
			return Math.toIntExact(StreamSupport.stream(students.spliterator(), false).count());
		}
	    
}
