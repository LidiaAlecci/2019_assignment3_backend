package org.pss2019.assignment3.repository;

import java.util.Date;
import java.util.Optional;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;

import org.pss2019.assignment3.helper.EntityManagerFactoryHelper;
import org.pss2019.assignment3.model.Professor;

public class ProfessorRepositoryImpl implements ProfessorRepository {

	public Optional<Professor> findById(Long id) {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		Professor prof = null;
		try {
		    entityManager.getTransaction().begin();
		    prof = entityManager.find(Professor.class, id);
		    entityManager.getTransaction().commit();
		    entityManager.close();
		    return Optional.ofNullable(prof);
		} catch (NoResultException e) {
	    	throw(e);
		}
	}
	
	public Iterable<Professor> findAll() {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    entityManager.getTransaction().begin();
	    Iterable<Professor> professors=entityManager.createQuery("FROM Professor", Professor.class).getResultList();
	    entityManager.getTransaction().commit();
	    entityManager.close();
	    return professors;
	}

	public Optional<Professor> findByFc(String fc) {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	    entityManager.getTransaction().begin();
	    Professor prof = (Professor) entityManager.createQuery("FROM Professor WHERE fc = '"+ fc.toString()+"'", Professor.class).getSingleResult();
	    entityManager.getTransaction().commit();
	    entityManager.close();
	    return Optional.ofNullable(prof);		
	}
	
	public void create(String fc,String firstName,
            String lastName, String city, Date birth) {
	  final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
	  Professor prof = null;
	  try {
		  entityManager.getTransaction().begin();
		  prof = new Professor(fc,firstName,lastName,city,birth);
		  entityManager.persist(prof);
		  entityManager.getTransaction().commit();
		  entityManager.close();  
	  } catch(RollbackException e) {
		  throw(e);
	  }
 }

	public void updateById(Long id, Professor prof) {
		if (id == prof.getId()) {
			final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
			entityManager.getTransaction().begin();
			Professor profOld = entityManager.find(Professor.class, id);
			if (profOld == null) {
				profOld = prof;
			}
			entityManager.merge(prof);
			entityManager.getTransaction().commit();
			entityManager.close();
		}
	}
	
	public void remove(Long id) {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		Professor prof = null;
		try {
			entityManager.getTransaction().begin();
			Optional<Professor> profOp= findById(id);
		    prof=profOp.get();		
	    	entityManager.remove(entityManager.contains(prof) ? prof : entityManager.merge(prof));
			entityManager.getTransaction().commit();
			entityManager.close();
	    } catch(NoResultException e){
			throw(e);
		}
	}
	
	public void removeAll() {
		final EntityManager entityManager = EntityManagerFactoryHelper.getFactory().createEntityManager();
		entityManager.getTransaction().begin();
		Iterable<Professor> professors = findAll();
		for(Professor p: professors) {
			entityManager.remove(entityManager.contains(p) ? p : entityManager.merge(p));
		}
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	public int size() {
		Iterable<Professor> professors = findAll();
		return Math.toIntExact(StreamSupport.stream(professors.spliterator(), false).count());
	}
}
