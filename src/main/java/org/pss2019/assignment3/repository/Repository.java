package org.pss2019.assignment3.repository;

import java.util.Optional;

public interface Repository<T, ID> {
  Optional<T> findById(ID id);
  Iterable<T> findAll();
  void updateById(ID id, T entity);
  void remove(ID id);
  void removeAll();
}
