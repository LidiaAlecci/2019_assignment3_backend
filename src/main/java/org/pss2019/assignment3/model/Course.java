package org.pss2019.assignment3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.JoinColumn;

@Entity
@Table(name = "courses")
public class Course {
	@Id
	@Column(name = "Code", nullable=false,columnDefinition = "varchar(50)", unique=true)
	private String code; //identification code
	@Column(name = "Name")
	private String name;
	
	@JoinColumn(name = "professor_id")
	@OneToOne
	@NotFound(action=NotFoundAction.IGNORE)
	private Professor professor;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="prerequisite_Courses", joinColumns=@JoinColumn(name="courseId"),
	 inverseJoinColumns=@JoinColumn(name="preRequisiteCourses")
	)
	private List<Course>  prerequisiteCourses;
	
	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	
	protected Course() {
		
	}
	
	public Course(String code, String name, List<Course> prerequisiteCourses, Professor professor) {
		super();
		this.code = code;
		this.name = name;
		this.prerequisiteCourses = prerequisiteCourses;
		this.professor = professor;
	}
	
	public Course(String code, String name) {
		super();
		this.code = code;
		this.name = name;
		this.prerequisiteCourses = new ArrayList<Course>();
	}
	
	public Course(String code, String name, Professor professor) {
		super();
		this.code = code;
		this.name = name;
		this.prerequisiteCourses = new ArrayList<Course>();
		this.professor = professor;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Course> getPrerequisiteCourses() {
		return prerequisiteCourses;
	}

	public void setPrerequisiteCourses(List<Course> prerequisiteCourses) {
		this.prerequisiteCourses = prerequisiteCourses;
	}
	
	public void addPreRequisite(Course preCourse) {
		this.prerequisiteCourses.add(preCourse);
	}

	@Override
	public String toString() {
		return "Course [code=" + code + ", name=" + name + ", professor=" + professor + ", prerequisiteCourses="
				+ prerequisiteCourses + "]";
	}
	
	


	
}

