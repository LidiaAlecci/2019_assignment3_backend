package org.pss2019.assignment3.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class Person {
	
	@Column(name = "FC", nullable=false,columnDefinition = "varchar(16)", unique=true)
	protected String fc; //fiscal code
	@Column(name = "FirstName")
  	protected String firstName;
	@Column(name = "LastName")
	protected String lastName;
	@Column(name = "Residency")
	protected String city;
	@Column(name = "Birth")
	@Temporal(TemporalType.TIMESTAMP)
    protected Date birth;

	
	public Person(String fc, String firstName, String lastName, String city, Date birth) {
		this.fc = fc;
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.birth = birth;
	}
	protected Person() {
		
	}
	@Override
	public String toString() {
		return "Person [fc=" + fc + ", firstName=" + firstName + ", lastName=" + lastName + ", city=" + city
				+ ", birth=" + birth + "]";
	}
	public String getFc() {
		return fc;
	}
	public void setFc(String fc) {
		this.fc = fc;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	
	
}
