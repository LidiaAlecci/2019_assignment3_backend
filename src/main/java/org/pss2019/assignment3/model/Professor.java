package org.pss2019.assignment3.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "professors")
public class Professor extends Person implements Serializable {
	private static final long serialVersionUID = -4488967030932037988L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", updatable=false, nullable=false)
	private Long id; //identification code
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Course> courses;
	
	protected Professor(){
		super();
	}
	
	public Professor(String fc, String firstName, String lastName, String city, Date birth) {
		super(fc, firstName, lastName, city, birth);
		this.courses= new ArrayList<Course>();
	}

	public Long getId() {
		return id;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "Professor [id=" + id  + ", fc=" + fc + ", firstName="
				+ firstName + ", lastName=" + lastName + ", city=" + city + ", birth=" + birth + "]";
	}

	public void addCourse(Course course) {
		this.courses.add(course);
	}
	
}
