package org.pss2019.assignment3.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import java.util.NoSuchElementException;

import javax.persistence.NoResultException;
import javax.persistence.RollbackException;

import org.pss2019.assignment3.model.Course;
import org.pss2019.assignment3.model.Student;
import org.pss2019.assignment3.repository.CourseRepositoryImpl;
import org.pss2019.assignment3.repository.StudentRepositoryImpl;

public class StudentTest {
	private static StudentRepositoryImpl sri = new StudentRepositoryImpl();
	private static CourseRepositoryImpl cri= new CourseRepositoryImpl();

	
	private Student s1;
	private Student s2;
	private Student s3;
	private Student s4;

	@BeforeEach
	public void init() {
		sri.create("AB1","Mario", "Rossi","Milano",new Date(0),2018L);
		sri.create("AB2","Mario", "Verdi","Firenze",new Date(0),2018L);
		sri.create("AB3","Mario", "Gialli","Milano",new Date(0),2018L);
		sri.create("AB4","Mario", "Azzurri","Egitto",new Date(0),2018L);
		
		cri.create("CC1","Esame1");
		cri.create("CC2","Esame2");
		cri.create("CC3", "Esame3");
		
		
		s1 =sri.findByFc("AB1").get();
		s2 =sri.findByFc("AB2").get();
		s3 =sri.findByFc("AB3").get();
		s4 =sri.findByFc("AB4").get();
	}
	
	@Test
	public void get() {
		Iterable<Student> students = sri.findAll();
		for (Student st: students) {
			assertNotEquals("Giacomo", st.getFirstName());
			assertNotEquals("5", sri.findByFc(st.getFc()));
		}
		assertEquals("Rossi", s1.getLastName());
	}
	

	@Test
	public void findById() {
		Long id=s3.getId();
		Long idtest=sri.findById(id).get().getId();
		assertEquals(id,idtest);
	}
	
	@Test
	public void update() {
		s4.setCity("Roma");
		sri.updateById(s4.getId(), s4);
		assertEquals("Roma", s4.getCity());
	}
	
	@Test
	public void remove() {
		assertEquals(4, sri.size());
		sri.remove(s2.getId());
		Iterable<Student> students = sri.findAll();	
		for (Student st: students) {
			assertNotEquals("Verdi", st.getLastName());
		}
		sri.remove(s1.getId());
		sri.remove(s3.getId());
		assertEquals(1, sri.size());
	}
	
	@Test
	 public void removeAll() {
		assertEquals(4,sri.size());
		sri.removeAll();
		assertEquals(0, sri.size());
	}
	
	@Test
	 public void removeAll2Times() {
		assertEquals(4,sri.size());
		sri.removeAll();
		assertEquals(0, sri.size());
		sri.removeAll();
		assertEquals(0, sri.size());
	}
	
	@Test
	public void testExceptionFindByFc() {
		Executable e= () -> {sri.findByFc("AB9").get();};
		 assertThrows(NoResultException.class, e);
	}

	@Test
	public void testExceptionCreate() {
		Executable e= () -> {sri.create("AB1","Mario", "Rossi","Milano",new Date(0),2018L);};
		assertThrows(RollbackException.class, e);
	}
	
	@Test
	public void testExceptionRemove() {
		sri.removeAll();
		Executable e= () -> {sri.remove(s2.getId());};
		assertThrows(NoSuchElementException.class, e);
	}	
	
	@Test 
	public void addCourse() {
		Course c3=cri.findById("CC3").get();
		s2.addCourse(c3);
		sri.updateById(s2.getId(), s2);
		assertEquals(1, s2.getCourses().size());

		Course c1=cri.findById("CC1").get();
		s2.addCourse(c1);
		sri.updateById(s2.getId(), s2);
		assertEquals(2, s2.getCourses().size());
	}
	
	@AfterEach
	public void clean() {
		sri.removeAll();
		cri.removeAll();
	}
	
}
