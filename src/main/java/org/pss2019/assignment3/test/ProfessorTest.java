/**
 * 
 */
package org.pss2019.assignment3.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import java.util.NoSuchElementException;

import javax.persistence.NoResultException;
import javax.persistence.RollbackException;

import org.pss2019.assignment3.model.Course;
import org.pss2019.assignment3.model.Professor;
import org.pss2019.assignment3.repository.CourseRepositoryImpl;
import org.pss2019.assignment3.repository.ProfessorRepositoryImpl;

public class ProfessorTest {

	private final ProfessorRepositoryImpl pri = new ProfessorRepositoryImpl();
	private final CourseRepositoryImpl cri= new CourseRepositoryImpl();
	
	private Professor p1;
	private Professor p2;
	private Professor p3;
	private Professor p4;

	@BeforeEach
	public void init() {
		pri.create("BB1","Alice", "Pittrice","Roma",new Date(0));
		pri.create("BB2","Alice", "Diario","Milano",new Date(0));
		pri.create("BB3","Alice", "Cornice","Bologna",new Date(0));
		pri.create("BB4","Alice", "Cacciatrice","Genova",new Date(0));
		
		cri.create("CC1","Esame1");
		cri.create("CC2","Esame2");
		cri.create("CC3", "Esame3");

		p1 =pri.findByFc("BB1").get();
		p2 =pri.findByFc("BB2").get();
		p3 =pri.findByFc("BB3").get();
		p4 =pri.findByFc("BB4").get();
	}
	
	@Test
	public void get() {
		Iterable<Professor> professors = pri.findAll();
		for (Professor prof: professors) {
			assertEquals("Alice", prof.getFirstName());
		}
		assertEquals("Pittrice", p1.getLastName());
		assertEquals("Cornice", p3.getLastName());
	}
	
	@Test
	public void findById() {
		Long id=p3.getId();
		Long idtest=pri.findById(id).get().getId();
		assertEquals(id,idtest);
	}
	
	@Test 
	public void update() {
		p1.setLastName("Perenice");
		pri.updateById(p1.getId(), p1);
		pri.updateById(p1.getId(), p2);
		Iterable<Professor> professors = pri.findAll();
		for (Professor prof: professors) {
			if (prof.getId()==1) {
				assertEquals("Perenice", prof.getLastName());
			}
		}	
	}
	
	@Test
	public void remove() {
		assertEquals(4, pri.size());
		pri.remove(p3.getId());
		assertEquals(3, pri.size());
		pri.remove(p1.getId());
		pri.remove(p2.getId());
		pri.remove(p4.getId());
		assertEquals(0, pri.size());
	}
	
	@Test
	 public void removeAll() {
		assertEquals(4, pri.size());
		pri.removeAll();
		assertEquals(0, pri.size());
	}
	
	@Test
	 public void removeAll2Times() {
		assertEquals(4, pri.size());
		pri.removeAll();
		assertEquals(0, pri.size());
		pri.removeAll();
		assertEquals(0, pri.size());
	}
	
	@Test 
	public void addCourse() {
		Course c3=cri.findById("CC3").get();
		p2.addCourse(c3);
		pri.updateById(p2.getId(), p2);
		assertEquals(1, p2.getCourses().size());

		Course c1=cri.findById("CC1").get();
		p2.addCourse(c1);
		pri.updateById(p2.getId(), p2);
		assertEquals(2, p2.getCourses().size());
	}
	
	@Test
	public void testExceptionFindByFc() {
		Executable e= () -> {pri.findByFc("BB9").get();};
		 assertThrows(NoResultException.class, e);
	}
	
	@Test
	public void testExceptionCreate() {
		Executable e= () -> {pri.create("BB1","Alice", "Pittrice","Roma",new Date(0));};
		assertThrows(RollbackException.class, e);
	}
	
	@Test
	public void testExceptionRemove() {
		pri.removeAll();
		Executable e= () -> {pri.remove(p2.getId());};
		assertThrows(NoSuchElementException.class, e);
	}
	
	@AfterEach
	public void clean() {
		pri.removeAll();
		cri.removeAll();
	}
}
