package org.pss2019.assignment3.test;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.RollbackException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.function.Executable;

import org.pss2019.assignment3.model.Course;
import org.pss2019.assignment3.model.Professor;
import org.pss2019.assignment3.repository.CourseRepositoryImpl;
import org.pss2019.assignment3.repository.ProfessorRepositoryImpl;

public class CourseTest {

	
	private CourseRepositoryImpl cri= new CourseRepositoryImpl();
	private Course c1;
	private Course c2;
	private Course c3;
	private Course c4;

	private ProfessorRepositoryImpl pri= new ProfessorRepositoryImpl();
	private Professor p1;
	
	@BeforeEach
	public void init() {
		
		cri.create("CC1","Esame1");
		cri.create("CC2","Esame2");
		cri.create("CC3", "Esame3");
		cri.create("CC4", "Esame4");

		c1=cri.findById("CC1").get();
		c2=cri.findById("CC2").get();
		c3=cri.findById("CC3").get();
		c4=cri.findById("CC4").get();
		
		pri.create("BB1","Alice", "Pittrice","Roma",new Date(0));
		p1=pri.findByFc("BB1").get();
	}
	
	@Test
	public void get() {
		Iterable<Course> courses = cri.findAll();
		for (Course c: courses) {
			assertNotEquals("CC5", c.getCode());
		}
		assertEquals("Esame3", c3.getName());
		assertNotEquals("Esame2", c3.getName());
	}

	@Test 
	public void update() {
		c1.setCode("CC0");
		c1.setName("Esame analisi");
		cri.updateById(c1.getCode(), c1);
		Iterable<Course> courses = cri.findAll();
		for (Course c: courses) {
			if (c.getName().contains("Esame analisi")) {
				assertEquals("CC0", c.getCode());
			}
		}	
	}
	
	@Test 
	public void remove() {
		cri.create("CC5", "Esame5");
		assertEquals(5, cri.size());
		cri.remove(c2.getCode());
		cri.remove(c1.getCode());
		cri.remove(c4.getCode());
		assertEquals(2, cri.size());		
	}
	
	@Test
	 public void removeAll() {
		assertEquals(4, cri.size());
		cri.removeAll();
		assertEquals(0,cri.size());
	}
	
	@Test
	 public void removeAll2Times() {
		assertEquals(4, cri.size());
		cri.removeAll();
		assertEquals(0,cri.size());
		cri.removeAll();
		assertEquals(0,cri.size());
	}
	
	@Test 
	public void addPrerequisites() {
		c4.addPreRequisite(cri.findById("CC1").get());
		c4.addPreRequisite(cri.findById("CC2").get());
		cri.updateById("CC4", c4);
		Course course = cri.findById("CC4").get();
		List<Course> preCourse=course.getPrerequisiteCourses();
		for (Course c: preCourse) {
			assertNotEquals("CC3", c.getCode());
		}
	}
	
	@Test 
	public void addTeacher() {
		c3.setProfessor(p1);
		cri.updateById(c3.getCode(), c3);
		assertEquals(p1.getFc(), c3.getProfessor().getFc());
	}
	
	@Test
	public void testExceptionFindById() {
		Executable e= () -> {cri.findById("CC7");};
		 assertThrows(NoResultException.class, e);
	}
	
	@Test
	public void testExceptionCreate() {
		Executable e= () -> {cri.create("CC1","Esame2");};
		assertThrows(RollbackException.class, e);
		e= () -> {cri.create("CC2","Esame8");};
		assertThrows(RollbackException.class, e);
	}
	
	@Test
	public void testExceptionRemove() {
		cri.removeAll();
		Executable e= () -> {cri.remove(c2.getCode());};
		 assertThrows(NoResultException.class, e);
	}
	
	@AfterEach
	public void clean() {
		cri.removeAll();
		pri.removeAll();
	}
}
