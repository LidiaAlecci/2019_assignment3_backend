package org.pss2019.assignment3.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryHelper {
	private static EntityManagerFactory emf;

    static {
        try {
        	emf = Persistence.createEntityManagerFactory("org.pss2019.assignment3");
        } catch(ExceptionInInitializerError e) {
            throw e;
        }
    }

    public static EntityManagerFactory getFactory() {
        return emf;
    }

}
